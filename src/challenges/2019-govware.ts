import { AnswersObject } from './type'

export const Answers: AnswersObject = [
  {
    title: "RSA NetWitness<sup>&reg;</sup> Platform",
    challenges: [
      "Evolved SIEM & Threat Defense",
      "End-to-end visibility",
      "Identify high-risk threats",
      "Reduce attacker dwell time"
    ],
    cssGridArea: '1 / 1 / 2 / 2'
  },
  {
    title: "RSA SecurID<sup>&reg;</sup> Suite",
    challenges: [
      "Identity & Access Assurance",
      "Enables a risk-based approach",
      "Mobile optimized",
      "Delivers convenience for users",
    ],
    cssGridArea: '1 / 2 / 2 / 3'
  },
  {
    title: "RSA Archer<sup>&reg;</sup> Suite",
    challenges: [
      "Integrated Risk Management",
      "accelerates decision-making",
      "improves efficiency",
      "drives accountability for risk",
    ],
    cssGridArea: '2 / 1 / 3 / 2'
  },
  {
    title: "RSA Fraud & Risk Intelligence Suite",
    challenges: [
      "Omni-Channel Fraud Prevention",
      "Protect 3D Secure transactions",
      "Phishing, malware, rouge mobile apps",
      "reduce ecommerce and mobile fraud",
    ],
    cssGridArea: '2 / 2 / 3 / 3'
  },
]
