export type AnswersObject = {
    title: string,
    challenges: string[],
    cssGridArea: string
}[]