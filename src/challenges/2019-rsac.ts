import { AnswersObject } from './type'

export const Answers: AnswersObject = [
  {
    title: "Product Solutions",
    challenges: [
      "Manage Digital Risk",
      "Digital Transformation",
      "Breakdown Silos",
      "Visibility Insight & Action",
      "Business-Driven Security"
    ],
    cssGridArea: '1 / 1 / 2 / 3'
  },
  {
    title: "Manage Third Party Risk",
    challenges: [
      "Third-Party Risk Assessment",
      "Real-Time Risk Treatment",
      "Know Your Business Impact"
    ],
    cssGridArea: '2 / 1 / 3 / 2'
  },
  {
    title: "Securing Your Cloud Transformation",
    challenges: [
      "Deep Cloud Visibility",
      "Manage Cloud Providers",
      "Secure Cloud Transformation"
    ],
    cssGridArea: '2 / 2 / 3 / 3'
  },
  {
    title: "Mitigate Cyber Attack Risk",
    challenges: [
      "Detect & Prioritize Threats",
      "Coordinate Business Response"
    ],
    cssGridArea: '3 / 1 / 3 / 2'
  },
  {
    title: "Manage Dynamic Workforce",
    challenges: [
      "Identity & Access Assurance",
      "Continuous User Monitoring"
    ],
    cssGridArea: '3 / 2 / 3 / 3'
  }
]