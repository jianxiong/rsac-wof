import './App.css';

import { OrderedSet, Set } from 'immutable';
import React, { Fragment } from 'react';

import { Answers } from './challenges/2019-rsac'
import OnScreenKeyboard from './OnScreenKeyboard';
import WofBoard from './WofBoard';
import _ from 'lodash';

type Penalty = {
  time: number;
  progress: number;
}

type State = {
  group: number
  answer: string
  revealed: OrderedSet<string>
  started: boolean
  timer: number
  timerText: string
  penalty: boolean
  penalties: Penalty[]
  completeTime: string | null
  timeout: boolean
  postGameReveal: boolean
  questionId: string
  dismissable: boolean
}

const GameTime = 60 * 1000

const SOUND_COUNTDOWN = new Audio("./sounds/countdown.mp3")
SOUND_COUNTDOWN.preload = "auto"

const SOUND_SOLVE = new Audio("./sounds/solve.mp3")
SOUND_SOLVE.preload = "auto"

const SOUND_BANKRUPT = new Audio("./sounds/bankrupt.mp3")
SOUND_BANKRUPT.preload = "auto"

new Image().src = "/complete.png"
new Image().src = "/timeout.png"

class App extends React.Component<{}, State> {
  randomAnswer = (group: number) => {
    let rand = _.random(0, Answers[group].challenges.length - 1)

    this.setState({
      questionId: "ABCDEF".charAt(group) + rand
    })
    return Answers[group].challenges[rand]
  }

  state: State = {
    group: -1,
    answer: "",
    revealed: OrderedSet<string>(),
    started: false,
    timer: 0,
    timerText: '60.0s',
    penalty: false,
    penalties: [],
    completeTime: null,
    timeout: false,
    postGameReveal: false,
    questionId: "",
    dismissable: false
  }

  newChar = (event: React.KeyboardEvent<HTMLInputElement>) => {
    let key = event.key.toUpperCase()
    if (key.length === 1) {
      this.setState({
        revealed: this.state.revealed.add(key)
      })
    }
  }

  handleAnswer = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      answer: event.target.value
    })
  }

  handleGroup = (group: number) => () => {
    this.setState({
      group,
      answer: this.randomAnswer(group),
      revealed: OrderedSet<string>(),
      timer: new Date().getTime() + GameTime,
      penalty: false,
      penalties: [],
      completeTime: null,
      started: true,
      timeout: false,
    })
  }

  handleGuess = (char: string) => () => {
    if (!this.state.revealed.has(char)) {
      this.setState({
        revealed: this.state.revealed.add(char)
      })
      setTimeout(() => { if (this.appRef) this.appRef.focus() }, 100)
    }
  }

  handleReset = () => {
    this.setState({
      group: -1,
      answer: "",
      revealed: OrderedSet<string>(),
      timer: 0,
      penalty: false,
      penalties: [],
      completeTime: null,
      started: false,
      timeout: false,
      postGameReveal: false,
      questionId: "",
    })
    SOUND_COUNTDOWN.pause()
  }

  handlePenalty = () => {
    this.setState({
      penalty: true
    })
  }

  appRef: HTMLDivElement | null = null

  componentDidMount() {
    setInterval(this.updateTimer, 100)
    if (this.appRef)
      this.appRef.focus()
  }

  updateTimer = () => {
    if (!this.state.started)
      return
    let curTime = new Date().getTime()
    let ms = this.state.timer - curTime
    if (ms <= 0) {
      this.setState({
        started: false,
        timeout: true,
        dismissable: false,
      })
      SOUND_COUNTDOWN.pause()
      SOUND_BANKRUPT.play()
      setTimeout(() => {
        this.setState({
          dismissable: true
        })
      }, 1000)
      return
    }

    if (ms <= 15000 && SOUND_COUNTDOWN.paused) {
      SOUND_COUNTDOWN.currentTime = 0
      SOUND_COUNTDOWN.play()
    }

    let penalty: Penalty = {
      time: 0,
      progress: 0,
    }
    if (this.state.penalty) {
      this.setState({
        penalty: false
      })
      penalty.time = this.penaltyFunction(this.state.penalties.length + 1, ms)
      penalty.progress = (1 - ms / GameTime) * 100

      let newTimer = Math.max(this.state.timer - penalty.time, curTime)
      this.setState({
        timer: newTimer
      })

      ms = newTimer - curTime
    }

    ms = Math.max(ms, 0)
    this.setState({
      timerText: (ms / 1000).toFixed(1) + 's',
      penalties: penalty.time > 0 ? [...this.state.penalties, penalty] : this.state.penalties
    })
  }

  penaltyFunction(n: number, ms: number): number {
    return 4000
  }

  handleKeyPress = (event: React.KeyboardEvent<HTMLDivElement>) => {
    let key = event.key.toUpperCase()
    console.log(key)
    if (key === "BACKSPACE") {
      if (this.state.revealed.count() > 0)
        this.setState({
          revealed: this.state.revealed.skipLast(1)
        })
      return
    }

    if (key === "ESCAPE") {
      this.handleEscape()
      return
    }
    if (key === "HOME") {
      this.handleReset()
      return
    }
    if (key === " ") {
      this.setState({
        started: !this.state.started,
      })
      return
    }
    if (key === "ENTER") {
      this.handleForceSolve()
    }

    if (key.length !== 1 || !"ABCDEFGHIJKLMNOPQRSTUVWXYZ".includes(key))
      return

    if (!this.state.revealed.has(key))
      this.setState({
        revealed: this.state.revealed.add(key)
      })
  }

  handleSolved = () => {
    SOUND_COUNTDOWN.pause()

    if (this.state.postGameReveal)
      return

    this.setState({
      started: false
    })
    let completeTime = ((GameTime - this.state.timer + new Date().getTime()) / 1000).toFixed(1)

    if (!this.state.postGameReveal) {
      setTimeout(() => {
        this.setState({
          completeTime,
          postGameReveal: true,
          dismissable: false,
        })
        SOUND_SOLVE.play()
        setTimeout(() => {
          this.setState({
            dismissable: true
          })
        }, 1000)
      }, 1500)
    }
  }

  handleEscape = () => {
    this.setState({
      postGameReveal: true,
      timeout: false,
      completeTime: null,
    })
    this.handleForceSolve()
  }

  handleForceSolve = () => {
    if (this.state.group >= 0 && this.state.completeTime === null)
      this.setState({
        revealed: this.state.revealed.union(Set<string>(this.state.answer.toUpperCase().split(""))),
        postGameReveal: this.state.timer - new Date().getTime() < 0
      })
  }

  render() {
    const { group, answer, revealed, timerText, penalties, completeTime, timeout, questionId, dismissable } = this.state

    let progressPercent = (100 - Math.max(this.state.timer - new Date().getTime(), 0) / GameTime * 100)

    return (
      <div className="App" tabIndex={0} onKeyDown={this.handleKeyPress} ref={(input) => { this.appRef = input; }}>
        {group < 0 ?
          <Fragment>
            <div className="main-logo">
              <img src={require('./rsa-logo.png')} />
            </div>
            {/*
            <div className="imageWorkforce">
              <a className="buttonWorkforce">Manage Dynamic Workforce Risk</a>
            </div>*/}
            <div className="suiteGrid">
              {
                Answers.map((v, i) => (
                  <a className={"suiteButton cell" + i} onClick={this.handleGroup(i)} key={i} style={{gridArea: v.cssGridArea}}>
                    <span dangerouslySetInnerHTML={{__html: v.title}}></span>
                  </a>
                )
                )
              }
            </div>
          </Fragment>
          : <Fragment>
            <div className="progress" style={{ left: `${progressPercent}%` }}>
              <div className="timer" style={{ transform: `translateX(-${progressPercent}%)` }}>
                {timerText}
              </div>
            </div>
            <div className="questionId">{questionId}</div>
            <div className="penalties">
              {
                penalties.map((p, k) =>
                  <div key={k} className="penalty fade" style={{ left: p.progress + '%', width: p.time / GameTime * 100 + "%" }}>-{p.time / 1000}s</div>
                )
              }
            </div>
            <WofBoard answer={answer} revealed={revealed} penalty={this.handlePenalty} solved={this.handleSolved} postGameReveal={this.state.postGameReveal} />
            <OnScreenKeyboard revealed={revealed} handleGuess={this.handleGuess} handleForceSolve={this.handleForceSolve} />
            <a className="logo" onClick={this.handleReset}>
              <img src={require('./rsa-logo.png')} />
            </a>
          </Fragment>
        }
        {completeTime ?
          <a className="modal complete" onClick={() => dismissable && this.handleEscape()}>
            <div className="completeTime">{completeTime} seconds</div>
            <div className="logo">
              <img src={require('./rsa-logo.png')} />
            </div>
          </a>
          : ''}
        {timeout ?
          <a className="modal timeout" onClick={() => dismissable && this.handleEscape()}>
            <div className="logo">
              <img src={require('./rsa-logo.png')} />
            </div>
          </a>
          : ''}
      </div>
    )
  }
}

export default App;
