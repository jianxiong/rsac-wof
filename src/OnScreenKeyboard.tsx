import { OrderedSet } from 'immutable';
import React from 'react'

type Props = {
  handleGuess(char: string): () => void
  handleForceSolve: () => void
  revealed: OrderedSet<string>
}

type State = {

}

const Alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

class OnScreenKeyboard extends React.Component<Props, State> {
  
  render() {
    return <div className="onscreenkeyboard">
      <div className="alphabets">
      {
        Alphabets.split("").map( (char) => 
          <button onClick={this.props.handleGuess(char)} className={`${this.props.revealed.has(char) ? 'revealed' : ''}`}>{ char.toUpperCase() }</button>
        )
      }
      </div>
      {/*<button onClick={this.props.handleForceSolve} className="solved">Solve</button>*/}
    </div>
  }
}

export default OnScreenKeyboard