import { OrderedSet } from 'immutable';
import React from 'react'
import _ from 'lodash'
import wrap from 'word-wrap'

type Props = {
  answer: string
  revealed: OrderedSet<string>
  penalty(): void
  solved(): void
  postGameReveal: boolean
}
type State = {
  board: string[]
  boardAnswer: string
  boardRevealed: OrderedSet<string> | null
}

const Alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

const SOUND_BUZZER = new Audio("./sounds/buzzer.mp3")
const SOUND_DING = new Audio("./sounds/ding.mp3")
SOUND_BUZZER.preload = "auto"
SOUND_DING.preload = "auto"

class WofBoard extends React.Component<Props, State> {
  state: State = {
    board: [],
    boardAnswer: "",
    boardRevealed: null,
  }
  renderCells = () => 
    this.state.board.map( (line, n) =>
      <div key={n} className="boardLine">
        {
          line.split("").map( (char, n) => 
            <div key={n} className={char === ' ' ? 'cell blank' : 'cell ' + (char !== '_' ? 'reveal' : '')}>
              { char !== ' ' ?
                <div className="inner">
                  <div className="back">
                  { char === '_' ? '' : char}
                  </div>
                </div>
              : ''
              }
            </div>
          )
        }
      </div> 
    )
  
  static getDerivedStateFromProps(props: Props, state: State) {
    if(props.revealed !== state.boardRevealed) {
      props.revealed.forEach(v => {
        if(Alphabets.includes(v) && (state.boardRevealed == null || !state.boardRevealed.has(v))) {
          if(!state.boardAnswer.toUpperCase().includes(v)) {
            props.penalty()
            if(!props.postGameReveal) {
              SOUND_BUZZER.pause()
              SOUND_BUZZER.currentTime = 0
              SOUND_BUZZER.play()
            }
          } else {
            if(!props.postGameReveal) {
              SOUND_DING.pause()
              SOUND_DING.currentTime = 0
              SOUND_DING.play()
            }
          }
        }
      })
    }
    if(props.answer !== state.boardAnswer || props.revealed !== state.boardRevealed) {
      let board = WofBoard.getBoard(props.answer, props.revealed)
      if(board.join("").indexOf("_") === -1)
        props.solved()
      return {
        board,
        boardAnswer: props.answer,
        boardRevealed: props.revealed
      }
    }
    else
      return null
  }
  static getBoard = (answer: string, revealed: OrderedSet<string>): string[] => {
    let board = wrap(answer.toUpperCase(), {width: 12, trim: true, indent: ''}).split("\n").map(x => x.trim())
    
    Alphabets.split("").forEach( (char: string) => {
      if(!revealed.has(char))
        board = board.map(x => x.replace(new RegExp(char, "g"), "_"))
    })
    
    return board
  }

  render() {
    return <div className="wof-board">
      { this.renderCells() }
    </div>
  }
}

export default WofBoard